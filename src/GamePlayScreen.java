/**
 * Created by jeffreyccching on 7/11/2016.
 */
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import javafx.application.Application;

public class GamePlayScreen extends Application {
    Button home;
    Button user;
    Label GameMode;
    Label Buzzword;
    Label Target;
    Label timeleft;
    Label total;
    //Label Level;
    Label targettrial;
    HBox timer;
    HBox displayLetter;
    Label Level;
    Label testWord;
    FlowPane pane1, pane2;
    Scene scene1, scene2;
    Stage thestage;
    VBox vboxLeft;
    VBox vboxRight;
    HBox hboxbottom;
    Button play;

    Button round1;
    Button round2;
    Button round3;
    Button round4;
    Button round5;
    Button round6;
    Button round7;
    Button round8;
    Button round9;
    Button round10;
    Button round11;
    Button round12;
    Button round13;
    Button round14;
    Button round15;
    Button round16;



    @Override
    public void start(Stage stage) {
        stage.setTitle("!!Buzzword!!");
        stage.setWidth(1200);
        stage.setHeight(800);
        Scene scene = new Scene(new Group());

        scene.getStylesheets().add("layout.css");
        home = new Button("home");
        user = new Button("user");
        GameMode = new Label("famous people");
        Buzzword = new Label("Buzz Word");

        home.setId(".button");


        user.setId(".buton");
        //profile.setStyle("-fx-pref-width: 100px;");
        //profile.setStyle("-fx-pref-height: 50px;");


        //home.getStyleClass().add("button");

        BorderPane border = new BorderPane();


        vboxLeft = new VBox();
        //vboxLeft.setPadding(new Insets(10));
        //vboxLeft.setSpacing(8);
        vboxLeft.setMargin(home, new Insets(40,15,15, 15));
        vboxLeft.setMargin(user, new Insets(0, 0, 0, 15));
        vboxLeft.setPadding(new Insets(0, 30, 30, 10));
        //vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(home);
        vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(user);

        vboxLeft.setStyle("-fx-background-color: #2e8b57;");
        //home.setStyle("-fx-pref-height: 100px;");


        border.setLeft(vboxLeft);




        vboxRight = new VBox();
        Target = new Label("Target : 75 points");

        vboxRight.setMargin(Target, new Insets(40,15,15, 15));
       // vboxRight.setMargin(user, new Insets(0, 0, 0, 15));
        vboxRight.setPadding(new Insets(0, 30, 30, 10));
        //vboxLeft.setSpacing(30);

        vboxRight.setSpacing(30);
        //vboxRight.getChildren().addAll(user);


        vboxRight.setStyle("-fx-background-color: #E5E4E2;");

        border.setRight(vboxRight);


        timer = new HBox();
        testWord = new Label("buy 45");
        total = new Label("Total: 45");
        displayLetter = new HBox();
        timeleft = new Label ("Time Remaining: 40 Sec");
        targettrial = new Label ("B U ");
        timer.setPadding(new Insets(15, 0, 0, 12));
        displayLetter.setStyle("-fx-background-color: #616D7E;");
        timer.getChildren().add(timeleft);
        displayLetter.getChildren().add(targettrial);



        vboxRight.getChildren().addAll(timer, displayLetter,  testWord,total,Target);






        hboxbottom = new HBox();
        hboxbottom.setStyle("-fx-background-color: #E5E4E2;");
        hboxbottom.setPadding(new Insets(15, 12, 15, 12));
        play = new Button("play");
        Level = new Label("Level 4");

        hboxbottom.getChildren().addAll(play, Level);
        hboxbottom.setSpacing(30);











        border.setBottom(hboxbottom);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(12);






        round1 = new Button (" B ");
        round2 = new Button ("  ");
        round3 = new Button ("");
        round4 = new Button ("");
        round5 = new Button (" T ");
        round6 = new Button (" Z ");
        round7 = new Button ("  ");
        round8 = new Button ("  ");
        round9 = new Button (" Y ");
        round10 = new Button ("  ");
        round11 = new Button (" W");
        round12 = new Button (" U ");
        round13 = new Button ("  ");
        round14 = new Button ("  ");
        round15 = new Button ("  ");
        round16 = new Button (" ");


        round1.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round2.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round3.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round4.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round5.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round6.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round7.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round8.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round9.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round10.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round11.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round12.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round13.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round14.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round15.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round16.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );



        grid.setMargin(round1, new Insets(0,0,0, 100));
        grid.setMargin(round2, new Insets(0,0,0, 220));
        grid.setMargin(round3, new Insets(0,0,0, 340));
        grid.setMargin(round4, new Insets(0,0,0, 460));
        grid.setMargin(round5, new Insets(170,0,0, 100));
        grid.setMargin(round6, new Insets(170,0,0, 220));
        grid.setMargin(round7, new Insets(170,0,0, 340));
        grid.setMargin(round8, new Insets(170,0,0, 460));
        grid.setMargin(round9, new Insets(330,0,0, 100));
        grid.setMargin(round10, new Insets(330,0,0, 220));
        grid.setMargin(round11, new Insets(330,0,0, 340));
        grid.setMargin(round12, new Insets(330,0,0, 460));
        grid.setMargin(round13, new Insets(500,0,0, 100));
        grid.setMargin(round14, new Insets(500,0,0, 220));
        grid.setMargin(round15, new Insets(500,0,0, 340));
        grid.setMargin(round16, new Insets(500,0,0, 460));












        HBox hGrid1 = new HBox();
        hGrid1.setMargin(Buzzword, new Insets(15,0,0, 200));
        //hGrid.setMargin(Buzzword, new Insets(0, 0, 0, 15));
        hGrid1.setMargin(GameMode, new Insets(70,0,0,-150));
        hGrid1.getChildren().addAll(Buzzword,GameMode);


        Buzzword.getStyleClass().add("text");


        grid.getChildren().addAll(hGrid1,round1,round2,round3,round4,round5,round6,round7,round8,round9,round10,round11,round12,round13,round14,round15,round16);

        border.setCenter(grid);


        scene.setRoot(border);

        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }


}
