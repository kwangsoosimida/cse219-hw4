/**
 * Created by jeffreyccching on 7/11/2016.
 */
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import javafx.application.Application;

public class homeScreen extends Application {
    Button Create;
    Button login;
    Button Start;
    //Label GameMode;
    Label Buzzword;
    FlowPane pane1, pane2;
    Scene scene1, scene2;
    Stage thestage;
    VBox vboxLeft;
    Button round1;
    Button round2;
    Button round3;
    Button round4;
    Button round5;
    Button round6;
    Button round7;
    Button round8;
    Button round9;
    Button round10;
    Button round11;
    Button round12;
    Button round13;
    Button round14;
    Button round15;
    Button round16;


    @Override
    public void start(Stage stage) {
        stage.setTitle("!!Buzzword!!");
        stage.setWidth(1200);
        stage.setHeight(800);
        Scene scene = new Scene(new Group());

        scene.getStylesheets().add("layout.css");






        final ComboBox GameSelect = new ComboBox();
        GameSelect.getItems().addAll(
                "famous people",
                "Dictionary",
                "places",
                "science"
        );









        Create = new Button("create profile");
        login = new Button("login");
        Start = new Button("start");
        //GameMode = new Label("famous people");
        Buzzword = new Label("Buzz Word");

        Create.setId(".button");
        //home.setStyle("-fx-pref-width: 100px;");
        //home.setStyle("-fx-pref-height: 50px;");

        login.setId(".buton");
        //profile.setStyle("-fx-pref-width: 100px;");
        //profile.setStyle("-fx-pref-height: 50px;");
        LoginScreen messageDialog   = LoginScreen.getSingleton();



        //loginStage
        //login.setOnAction(e -> messageDialog.show("Login","Login"));
        login.setOnAction(e -> loginStage());

        //home.getStyleClass().add("button");

        BorderPane border = new BorderPane();
        //HBox hbox = addHBox();
        //border.setTop(hbox);

        //addStackPane(hbox);         // Add stack to HBox in top region

        vboxLeft = new VBox();
        //vboxLeft.setPadding(new Insets(10));
        //vboxLeft.setSpacing(8);
        vboxLeft.setMargin(Create, new Insets(40,15,15, 15));
        vboxLeft.setMargin(login, new Insets(0, 0, 0, 15));
        vboxLeft.setMargin(Start, new Insets(0, 0, 0, 15));
        vboxLeft.setPadding(new Insets(0, 30, 30, 10));
        //vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(Create);
        vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(login);
        vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(Start,GameSelect);
        //vboxLeft.getChildren().add(profile);
        //vboxLeft.getStyleClass().add("vbox");
        //vboxLeft.setId("vbox");
        vboxLeft.setStyle("-fx-background-color: #2e8b57;");
        //home.setStyle("-fx-pref-height: 100px;");


        border.setLeft(vboxLeft);




        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(12);


        round1 = new Button (" B ");
        round2 = new Button (" U ");
        round3 = new Button ("");
        round4 = new Button ("");
        round5 = new Button (" Z ");
        round6 = new Button (" Z ");
        round7 = new Button ("  ");
        round8 = new Button ("  ");
        round9 = new Button ("  ");
        round10 = new Button ("  ");
        round11 = new Button (" W");
        round12 = new Button (" O ");
        round13 = new Button ("  ");
        round14 = new Button ("  ");
        round15 = new Button (" R ");
        round16 = new Button (" D ");


        round1.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round2.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round3.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round4.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round5.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round6.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round7.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round8.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round9.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round10.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round11.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round12.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round13.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round14.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round15.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round16.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );



        grid.setMargin(round1, new Insets(0,0,0, 100));
        grid.setMargin(round2, new Insets(0,0,0, 220));
        grid.setMargin(round3, new Insets(0,0,0, 340));
        grid.setMargin(round4, new Insets(0,0,0, 460));
        grid.setMargin(round5, new Insets(170,0,0, 100));
        grid.setMargin(round6, new Insets(170,0,0, 220));
        grid.setMargin(round7, new Insets(170,0,0, 340));
        grid.setMargin(round8, new Insets(170,0,0, 460));
        grid.setMargin(round9, new Insets(330,0,0, 100));
        grid.setMargin(round10, new Insets(330,0,0, 220));
        grid.setMargin(round11, new Insets(330,0,0, 340));
        grid.setMargin(round12, new Insets(330,0,0, 460));
        grid.setMargin(round13, new Insets(500,0,0, 100));
        grid.setMargin(round14, new Insets(500,0,0, 220));
        grid.setMargin(round15, new Insets(500,0,0, 340));
        grid.setMargin(round16, new Insets(500,0,0, 460));
        grid.getChildren().addAll(round1,round2,round3,round4,round5,round6,round7,round8,round9,round10,round11,round12,round13,round14,round15,round16);





        HBox hGrid1 = new HBox();
        hGrid1.setMargin(Buzzword, new Insets(15,0,0, 400));
        //hGrid.setMargin(Buzzword, new Insets(0, 0, 0, 15));
        //hGrid1.setMargin(GameMode, new Insets(70,0,0,-150));
        hGrid1.getChildren().addAll(Buzzword);


        Buzzword.getStyleClass().add("text");
        //GameMode.setStyle(" -fx-text-align: center;");
        //GameMode.setStyle("-fx-font-size: 40px;");
        //GameMode.setStyle("-fx-font-style: italic;");

        //grid.setMargin(GameMode, new Insets(80,40,40, 420));
        // grid.setMargin(Buzzword, new Insets(0,40,80, 440));
        //grid.getChildren().addAll(Buzzword,GameMode);

        grid.getChildren().addAll(hGrid1);

        border.setCenter(grid);


        //grid.setPrefWrapLength(300); // preferred width = 300

        //grid.getChildren().add(home);

        scene.setRoot(border);

        stage.setScene(scene);
        stage.show();
    }
    public void loginStage(){
        Stage newStage = new Stage();
        VBox comp = new VBox();
        TextField nameField = new TextField("user name");
        TextField phoneNumber = new TextField("password");
        comp.getChildren().add(nameField);
        comp.getChildren().add(phoneNumber);

        Scene stageScene = new Scene(comp, 300, 300);
        newStage.setScene(stageScene);
        newStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }


}
