/**
 * Created by jeffreyccching on 7/11/2016.
 */
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.Insets;
import javafx.application.Application;

public class LevelSelectionScreem extends Application {
    Button home;
    Button profile;
    Label GameMode;
    Label Buzzword;
    FlowPane pane1, pane2;
    Scene scene1, scene2;
    Stage thestage;
    VBox vboxLeft;
    Button round1;
    Button round2;
    Button round3;
    Button round4;
    Button round5;
    Button round6;
    Button round7;
    Button round8;


    @Override
    public void start(Stage stage) {
        stage.setTitle("!!Buzzword!!");
        stage.setWidth(1200);
        stage.setHeight(800);
        Scene scene = new Scene(new Group());

        scene.getStylesheets().add("layout.css");
        home = new Button("home");
        profile = new Button("profile");
        GameMode = new Label("famous people");
        Buzzword = new Label("Buzz Word");

        home.setId(".button");
        //home.setStyle("-fx-pref-width: 100px;");
        //home.setStyle("-fx-pref-height: 50px;");

        profile.setId(".buton");
        //profile.setStyle("-fx-pref-width: 100px;");
        //profile.setStyle("-fx-pref-height: 50px;");


        //home.getStyleClass().add("button");

        BorderPane border = new BorderPane();
        //HBox hbox = addHBox();
        //border.setTop(hbox);

        //addStackPane(hbox);         // Add stack to HBox in top region

        vboxLeft = new VBox();
        //vboxLeft.setPadding(new Insets(10));
        //vboxLeft.setSpacing(8);
        vboxLeft.setMargin(home, new Insets(40,15,15, 15));
        vboxLeft.setMargin(profile, new Insets(0, 0, 0, 15));
        vboxLeft.setPadding(new Insets(0, 30, 30, 10));
        //vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(home);
        vboxLeft.setSpacing(30);
        vboxLeft.getChildren().addAll(profile);
        //vboxLeft.getChildren().add(profile);
        //vboxLeft.getStyleClass().add("vbox");
        //vboxLeft.setId("vbox");
        vboxLeft.setStyle("-fx-background-color: #2e8b57;");
        //home.setStyle("-fx-pref-height: 100px;");


        border.setLeft(vboxLeft);

        round1 = new Button (" 1 ");
        round2 = new Button (" 2 ");
        round3 = new Button (" 3 ");
        round4 = new Button (" 4 ");
        round5 = new Button (" 5 ");
        round6 = new Button (" 6 ");
        round7 = new Button (" 7 ");
        round8 = new Button (" 8 ");

        round4.setDisable(true);

        round1.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );

        round2.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round3.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round4.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round5.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round6.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round7.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );
        round8.setStyle(
                "-fx-background-radius: 5em; " +
                        "-fx-min-width: 80px; " +
                        "-fx-min-height: 80px; " +
                        "-fx-max-width: 80px; " +
                        "-fx-max-height: 80px;"
        );


        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(12);


        HBox hGrid1 = new HBox();
        hGrid1.setMargin(Buzzword, new Insets(15,0,0, 400));
        //hGrid.setMargin(Buzzword, new Insets(0, 0, 0, 15));
        hGrid1.setMargin(GameMode, new Insets(70,0,0,-150));
        hGrid1.getChildren().addAll(Buzzword,GameMode);


        Buzzword.getStyleClass().add("text");
        //GameMode.setStyle(" -fx-text-align: center;");
        //GameMode.setStyle("-fx-font-size: 40px;");
        //GameMode.setStyle("-fx-font-style: italic;");

        //grid.setMargin(GameMode, new Insets(80,40,40, 420));
       // grid.setMargin(Buzzword, new Insets(0,40,80, 440));
        //grid.getChildren().addAll(Buzzword,GameMode);

        grid.setMargin(round1, new Insets(140,0,0, 100));
        grid.setMargin(round2, new Insets(140,0,0, 220));
        grid.setMargin(round3, new Insets(140,0,0, 340));
        grid.setMargin(round4, new Insets(140,0,0, 460));
        grid.setMargin(round5, new Insets(310,0,0, 100));
        grid.setMargin(round6, new Insets(310,0,0, 220));
        grid.setMargin(round7, new Insets(310,0,0, 340));
        grid.setMargin(round8, new Insets(310,0,0, 460));
        grid.getChildren().addAll(hGrid1,round1,round2,round3,round4,round5,round6,round7,round8);

        border.setCenter(grid);


        //grid.setPrefWrapLength(300); // preferred width = 300

        //grid.getChildren().add(home);

        scene.setRoot(border);

        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }


}
